import 'package:flutter/material.dart';
import 'package:flutter/semantics.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ShopPage extends StatefulWidget {
  ShopPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState()=> _ShopPageState();
}
class _ShopPageState extends State<ShopPage>{
  String _title = 'Himdeve Shop';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body: _buildWebView(),
      floatingActionButton: _buildChangeTitleBtn(),
    );
  }

  Widget _buildWebView() {
    return WebView(
      javascriptMode: JavascriptMode.unrestricted,
      initialUrl: 'https://himdeve.eu',
    );
  }
  Widget _buildChangeTitleBtn(){
    return FloatingActionButton(
      
      onPressed: (){
        setState((){
          _title = 'Himdeve Development Tutorial';
        });
      },
      child: Icon(Icons.title),
      );
  }
}
